# README #

Generalized Stochastic Petri Net benchmark

### What is this repository for? ###

* This repository includes a benchmark of GSPN models. It has been used for comparing different simulators implemented in different tools: GreatSPN - PeaBrain - TimeNET. 
  The repository is organized accordingly (one folder per tool).
  In order to evaluate the efficiency and correctness of the simulators we consider different workload GSPN  models: 
  LOW / MEDIUM / HIGH. The basic set of models is included in the LOW folder: there are a total of **50** different models. 
  The MEDIUM and HIGH folders includes variants of the basic set, that is same model but different initial marking.
  See the wiki pages for detailed info.
* Version: V0